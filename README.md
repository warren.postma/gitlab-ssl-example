# gitlab-ssl-example


## Gitlab Omnibus Configuration / Ubuntu / HTTPS

This project is just a container for the gitlab.rb file. The gitlab.rb file provided:

* Is known to work, on Ubuntu 14 and 16 LTS and has HTTPS with a signed (not self signed) certificate.

* Is integrated with an Active Directory domain (via LDAP)

* Has mattermost enabled, and shows the trust (bogus secrets embedded will not work for you) configured

* Has gitlab and mattermost on listening on HTTPS via NGINX 

* Has a trivial SMTP server integration configured, assuming an open-smtp-relay (no TLS). I might change this in the future because even on a private LAN open SMTP relays are an ugly thing in 2016.

# DO NOT TRY TO USE SELF SIGNED CERTIFICATES. You're welcome.

* https://gitlab.com/warren.postma/gitlab-ssl-example/blob/master/gitlab.rb

This projects existence does not constitute not an offer to help you fix your gitlab. 
I hope it's useful but I'm not your IT guy. 
If you think I am leading people astray in an way, please make a pull request (MR) this project. 

If you email me at warren.postma@gmail.com asking for help, support, clarifications, or ideas on how to fix your gitlab, your email will be deleted. I do not scale.
